package com.example.batchprocessing.Model;

public class Item {
    private int id;
    private double preco;
    private int qtdItem;

    Item() {}

    public Item(int id, double preco, int qtdItem) {
        this.id = id;
        this.preco = preco;
        this.qtdItem = qtdItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getQtdItem() {
        return qtdItem;
    }

    public void setQtdItem(int qtdItem) {
        this.qtdItem = qtdItem;
    }
}
