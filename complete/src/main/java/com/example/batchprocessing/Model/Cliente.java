package com.example.batchprocessing.Model;

public class Cliente {
    public static final int id = 002;

    private String nome;
    private String cnpj;
    private String businessArea;

    Cliente() {}

    public Cliente(String nome, String cnpj, String businessArea) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.businessArea = businessArea;
    }

    public static int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }



}
