package com.example.batchprocessing.Model;

public class ExampleText {

    private String texto;

    public ExampleText() {}

    public ExampleText(String text) {
        this.texto = text;
    }


    public String getTexto() {
        return texto;
    }

    public void setTexto(String testo) {
        this.texto = testo;
    }
}
