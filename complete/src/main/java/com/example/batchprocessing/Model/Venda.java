package com.example.batchprocessing.Model;

import java.util.ArrayList;

public class Venda {
    public static int id = 003;

    private String saleId;
    private ArrayList<Item> itens;
    private Vendedor vendedor;

    Venda() {}

    public Venda(String saleId, ArrayList<Item> itens) {
        this.saleId = saleId;
        this.itens = itens;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Venda.id = id;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public ArrayList<Item> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Item> itens) {
        this.itens = itens;
    }
}
