package com.example.batchprocessing.Config;

import com.example.batchprocessing.Model.ExampleText;
import com.example.batchprocessing.Processor.TextoItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.time.LocalDateTime;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Bean
	public FlatFileItemReader<ExampleText> reader() {

		return new FlatFileItemReaderBuilder<ExampleText>()
				.name("ExampleTextItemBuilder")
				.resource(new FileSystemResource("data/in/lote.dat"))
				.delimited()
				.names(new String[]{ "texto" })
				.fieldSetMapper(new BeanWrapperFieldSetMapper<ExampleText>() {{
					setTargetType(ExampleText.class);
				}})
				.build();
	}

	@Bean
	public TextoItemProcessor processor() {
		return new TextoItemProcessor();
	}

	@Bean
	public ItemWriter<ExampleText> writer() {
		FlatFileItemWriter<ExampleText> writer = new FlatFileItemWriter<>();

		Resource output = new FileSystemResource("data/out/"+ LocalDateTime.now() +".dat");

		writer.setResource(output);

		//TRUE APEND NO ARQUIVO DE SAIDA, FALSE PARA CRIAR NOVO
		writer.setAppendAllowed(false);

		writer.setLineAggregator(new DelimitedLineAggregator<ExampleText>() {
			{
				setDelimiter("");
				setFieldExtractor(new BeanWrapperFieldExtractor<ExampleText>() {
					{
						setNames(new String[] { "texto" });
					}
				});
			}
		});
		return writer;
	}

	@Bean
	public Job importDataJob( Step step1 ) {
		return jobBuilderFactory.get("importUserJob")
			.incrementer(new RunIdIncrementer())
			.flow(step1)
			.end()
			.build();
	}

	@Bean
	public Step step1(ItemWriter writer) {
		return stepBuilderFactory.get("step1")
			.<ExampleText, ExampleText>chunk(1)
			.reader(reader())
			.processor(processor())
			.writer(writer)
			.build();
	}
}
