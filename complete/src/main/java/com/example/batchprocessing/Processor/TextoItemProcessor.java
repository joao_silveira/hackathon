package com.example.batchprocessing.Processor;

import com.example.batchprocessing.Model.Cliente;
import com.example.batchprocessing.Model.ExampleText;
import com.example.batchprocessing.Model.Vendedor;
import com.example.batchprocessing.Util.IdentificadorDeLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class TextoItemProcessor implements ItemProcessor<ExampleText, ExampleText> {

    private static final Logger log = LoggerFactory.getLogger(TextoItemProcessor.class);

    @Override
    public ExampleText process(final ExampleText texto) throws Exception {

        String oldText = texto.getTexto();

        String transformedText = "";

        IdentificadorDeLayout identificadorDeLayout = new IdentificadorDeLayout();

        String[] aux = oldText.split("ç");

        if(identificadorDeLayout.identifica(oldText).equals("vendedor")) {
            Vendedor vendedor = new Vendedor(aux[2], aux[1], Double.parseDouble(aux[3]));
            transformedText = "Vendedor: "+ vendedor.getNome() + ", CPF: " + vendedor.getCpf() +", Salario: "+ vendedor.getSalario();
        } else if (identificadorDeLayout.identifica(oldText).equals("cliente")) {
            Cliente cliente = new Cliente(aux[2], aux[1], aux[3]);
            transformedText = "Cliente: "+ cliente.getNome() + ", CNPJ: " + cliente.getCnpj() +", Area de atuacao: "+ cliente.getBusinessArea();
        }

        log.info(transformedText);

        ExampleText transformedTextObj = new ExampleText(transformedText);
        return transformedTextObj;
    }
}