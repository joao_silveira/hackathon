![](https://scontent.fpoa1-1.fna.fbcdn.net/v/t1.0-9/38690991_293028781447267_8949017828158078976_n.png?_nc_cat=105&_nc_sid=85a577&_nc_ohc=m_6NTKiaxC8AX99Jvir&_nc_ht=scontent.fpoa1-1.fna&oh=d0188df84ec922c7c2198b4ab884cf47&oe=5F95EFC9)
# Projeto Mini-Hackathon Vem Ser 2020
## O projeto é composto por duas aplicações: a primeira é um sistema que analisa lotes de dados .dat e retorna um relatório dos mesmos.O segundo é uma API com dois tipos de usuário, analista e consultor, o consultor lê um relatório .dat e persiste no sistema e o analista consulta os dados persistidos no banco. 

![Badge](https://img.shields.io/static/v1.svg?label=License&message=MIT&color=blue)

Tabela de conteúdos
=================

   - [Status]
   - [Features]
   - [Demonstração da aplicação]
   - [Pré-Requisitos]
   - [Tecnologias Utilizadas]
   - [Autores]
   - [Licença]

### Status

- Em desenvolvimento

### Features
 
- Consulta a pasta /data/in e gera um relatório a partir dos lotes recebidos
- Armazena o relatório no repositório /data/out
- Envia relatórios para o banco de dados 
- Permite a consulta dos dados 

### Demonstração da aplicação

- ![](https://cdn.discordapp.com/attachments/759206767879520276/759930321289609246/DiagramaServico_1_1.jpg)

### Pré-Requisitos

Antes de começar, você vai precisará instalar as seguintes ferramentas:
[Git](https://gitforwindows.org/), [IntelliJ IDEA](https://www.jetbrains.com/pt-br/idea/), [Docker](https://www.docker.com/products/docker-desktop) 

### Tecnologias Utilizadas

As seguintes ferramentas foram usadas na construção do projeto:

- [IntelliJ IDEA](https://www.jetbrains.com/pt-br/idea/)
- [Docker](https://www.docker.com/products/docker-desktop)
- [SublimeText](https://www.sublimetext.com/)
- [BitBucket](https://bitbucket.org/)
- [Spring Batch](https://spring.io/projects/spring-batch)

### Autores

- Arthur Mattos
- Henrique Klein
- João Silveira

### MIT License

Copyright (c) <2020> <Arthur Mattos, Henrique Klein & João Silveira>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
